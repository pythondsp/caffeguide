.. Caffe Guide documentation master file, created by
   sphinx-quickstart on Fri Dec  7 08:42:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Caffe Guide
================

.. toctree::
    :maxdepth: 3
    :numbered:
    :includehidden:
    :caption: Contents:

    caffe/basic

