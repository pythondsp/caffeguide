Caffe Tutorial 
**************



LeNet
=====

In this section, the LeNet algorithm is applied on the 'mnist' dataset. 

:download:`click here<data/mnist_caffe.tar.gz>` to download the required files. 



Caffe model
-----------


* First convert the dataset into .mdb format which is read by Caffe model. Here we have two datasets i.e. for training and testing which are saved in folder 'mnist_train_lmdb' and 'mnist_test_lmdb' respectively. Below is the folder strcuture for this tutorial. 

.. note:: 

	* The .mdb files are generated according to instrcution in http://caffe.berkeleyvision.org/gathered/examples/mnist.html. 
	* Also, other files are taken from the same webpage and 'locations of files and folder' are modified accoding to below folder strcuture. 



.. code-block:: text

	.
	├── lenet_solver.prototxt
	├── lenet_train_test.prototxt
	├── mnist_test_lmdb
	│   ├── data.mdb
	│   └── lock.mdb
	├── mnist_train_lmdb
	│   ├── data.mdb
	│   └── lock.mdb
	└── train_lenet.sh


* Next, define a model in 'prototxt' file, e.g. in below code ('lenet_train_test.prototxt'), a LeNet nework is defined which reads the two dataset i.e. mnist_train_lmdb and mnist_test_lmdb, 


.. code-block:: python
    :linenos:
    :name: Lenet model using Caffe

    # lenet_train_test.prototxt

    name: "LeNet"
    layer {
      name: "mnist"
      type: "Data"
      top: "data"
      top: "label"
      include {
        phase: TRAIN
      }
      transform_param {
        scale: 0.00390625
      }
      data_param {
        source: "mnist_train_lmdb"
        batch_size: 64
        backend: LMDB
      }
    }
    layer {
      name: "mnist"
      type: "Data"
      top: "data"
      top: "label"
      include {
        phase: TEST
      }
      transform_param {
        scale: 0.00390625
      }
      data_param {
        source: "mnist_test_lmdb"
        batch_size: 100
        backend: LMDB
      }
    }
    layer {
      name: "conv1"
      type: "Convolution"
      bottom: "data"
      top: "conv1"
      param {
        lr_mult: 1
      }
      param {
        lr_mult: 2
      }
      convolution_param {
        num_output: 20
        kernel_size: 5
        stride: 1
        weight_filler {
          type: "xavier"
        }
        bias_filler {
          type: "constant"
        }
      }
    }
    layer {
      name: "pool1"
      type: "Pooling"
      bottom: "conv1"
      top: "pool1"
      pooling_param {
        pool: MAX
        kernel_size: 2
        stride: 2
      }
    }
    layer {
      name: "conv2"
      type: "Convolution"
      bottom: "pool1"
      top: "conv2"
      param {
        lr_mult: 1
      }
      param {
        lr_mult: 2
      }
      convolution_param {
        num_output: 50
        kernel_size: 5
        stride: 1
        weight_filler {
          type: "xavier"
        }
        bias_filler {
          type: "constant"
        }
      }
    }
    layer {
      name: "pool2"
      type: "Pooling"
      bottom: "conv2"
      top: "pool2"
      pooling_param {
        pool: MAX
        kernel_size: 2
        stride: 2
      }
    }
    layer {
      name: "ip1"
      type: "InnerProduct"
      bottom: "pool2"
      top: "ip1"
      param {
        lr_mult: 1
      }
      param {
        lr_mult: 2
      }
      inner_product_param {
        num_output: 500
        weight_filler {
          type: "xavier"
        }
        bias_filler {
          type: "constant"
        }
      }
    }
    layer {
      name: "relu1"
      type: "ReLU"
      bottom: "ip1"
      top: "ip1"
    }
    layer {
      name: "ip2"
      type: "InnerProduct"
      bottom: "ip1"
      top: "ip2"
      param {
        lr_mult: 1
      }
      param {
        lr_mult: 2
      }
      inner_product_param {
        num_output: 10
        weight_filler {
          type: "xavier"
        }
        bias_filler {
          type: "constant"
        }
      }
    }
    layer {
      name: "accuracy"
      type: "Accuracy"
      bottom: "ip2"
      bottom: "label"
      top: "accuracy"
      include {
        phase: TEST
      }
    }
    layer {
      name: "loss"
      type: "SoftmaxWithLoss"
      bottom: "ip2"
      bottom: "label"
      top: "loss"
    }



* Now define a solver file which contains various settings for the training e.g. learning rate (i.e. base_lr), maximum iteration, and snapshot: 5000 (i.e. save model after every 5000 iteration) etc. 

.. code-block:: python
    :linenos:
    :name: Solver file

    # lenet_solver.prototxt

    # The train/test net protocol buffer definition
    net: "lenet_train_test.prototxt"

    # test_iter specifies how many forward passes the test should carry out.
    # In the case of MNIST, we have test batch size 100 and 100 test iterations,
    # covering the full 10,000 testing images.
    test_iter: 100

    # Carry out testing every 500 training iterations.
    test_interval: 500
    # The base learning rate, momentum and the weight decay of the network.
    base_lr: 0.01
    momentum: 0.9
    weight_decay: 0.0005

    # The learning rate policy
    lr_policy: "inv"
    gamma: 0.0001
    power: 0.75

    # Display every 100 iterations
    display: 100

    # The maximum number of iterations
    max_iter: 10000

    # snapshot intermediate results
    snapshot: 5000
    snapshot_prefix: "lenet"

    # solver mode: CPU or GPU
    solver_mode: CPU


* Run the lenet_solver.prototxt file using below script. 

.. code-block:: shell
    :linenos:
    :name: train_lenet.sh

    #!/usr/bin/env sh

    # train_lenet.sh

    set -e

    /home/meherp/caffe/build/tools/caffe train --solver=lenet_solver.prototxt $@




Run the above file and it will generate the 'caffemodel' files. Two .caffemodel files are generated as we set the max_iter=10000 and snapshot=5000. Therefore one is saved at iteration = 5000 and next at iteration=10000. Below is the folder strcuture after running the above command, 

.. code-block:: shell

    ./train_lenet.sh


    # Below is the final output of above command 
    # [...]
    # I0316 21:39:56.600476 14282 solver.cpp:464] Snapshotting to binary proto file lenet_iter_10000.caffemodel
    # I0316 21:39:56.606088 14282 sgd_solver.cpp:284] Snapshotting solver state to binary proto file lenet_iter_10000.solverstate
    # I0316 21:39:56.640134 14282 solver.cpp:327] Iteration 10000, loss = 0.00216418
    # I0316 21:39:56.640166 14282 solver.cpp:347] Iteration 10000, Testing net (#0)
    # I0316 21:40:00.619458 14286 data_layer.cpp:73] Restarting data prefetching from start.
    # I0316 21:40:00.771006 14282 solver.cpp:414]     Test net output #0: accuracy = 0.9905
    # I0316 21:40:00.771044 14282 solver.cpp:414]     Test net output #1: loss = 0.0282565 (* 1 = 0.0282565 loss)
    # I0316 21:40:00.771060 14282 solver.cpp:332] Optimization Done.
    # I0316 21:40:00.771064 14282 caffe.cpp:250] Optimization Done.



.. code-block:: text


    .
    ├── lenet_iter_10000.caffemodel
    ├── lenet_iter_10000.solverstate
    ├── lenet_iter_5000.caffemodel
    ├── lenet_iter_5000.solverstate
    ├── lenet_solver.prototxt
    ├── lenet_train_test.prototxt
    ├── mnist_test_lmdb
    │   ├── data.mdb
    │   └── lock.mdb
    ├── mnist_train_lmdb
    │   ├── data.mdb
    │   └── lock.mdb
    └── train_lenet.sh



Deephi model
------------


* Run the decent and dnnc command 

 
.. code-block:: text

 	decent quantize -model lenet_train_test.prototxt -weights lenet_iter_10000.caffemodel

 	cd quantize_results

 	dnnc --prototxt deploy.prototxt --caffemodel deploy.caffemodel --dpu 1024FA --cpu_arch=arm64 --output_dir "final" --net_name "lenet"



* Below is the output of dnnc command and see the generated graph in :numref:`fig_lenet_meher_kernel_graph`
 

.. code-block:: text

    [DNNC][Warning] layer [loss] is not supported in DPU, deploy it in CPU instead.

    DNNC Kernel Information

    1. Overview
    kernel numbers  : 2
    kernel topology : lenet_meher_kernel_graph.jpg

    2. Kernel Description in Detail
    kernel id       : 0
    kernel name     : lenet_meher_0
    type            : DPUKernel
    nodes           : NA
    input node(s)   : conv1(0) 
    output node(s)  : ip2(0) 

    kernel id       : 1
    kernel name     : lenet_meher_1
    type            : CPUKernel
    nodes           : NA
    input node(s)   : loss 
    output node(s)  : loss 


.. _`fig_lenet_meher_kernel_graph`:

.. figure:: img/lenet_meher_kernel_graph.jpg

   lenet_meher_kernel_graph.jpg 




.. error:: 

	Above results shows two files, but if we see the folder 'final' then there is only one file i.e. 'dpu_lenet_meher_0.elf' (not the other). 


